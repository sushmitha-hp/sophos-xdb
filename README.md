# Sophos-xdb

** analyzing student score data using elasticsearch and kibana **


- To install Elasticsearch (version 7.8.1) for windows:-

Download zip file from below link

`https://artifacts.elastic.co/downloads/elasticsearch/elasticsearch-7.8.1-windows-x86_64.zip`

Unzip this folder. This will create a folder called elasticsearch-7.8.1-windows-x86_64

- To start elasticsearch through  terminal:--

Navigate to the folder containing elasticsearch

`cd c:\elasticsearch-7.8.1-windows-x86_64`

To start elasticsearch

`.\bin\elasticsearch.bat`


To Stop elasticsearch

`Ctrl-C`

To test Elasticsearch is running in 9200 on localhost

`http://localhost:9200/`

- To install kibana (version 7.8.1) for windows:-

Download zip package from below link

`https://artifacts.elastic.co/downloads/kibana/kibana-7.8.1-windows-x86_64.zip`

Unzip the folder. This will create a folder called kibana-7.8.1-windows-x86_64

- To start kibana through  terminal:--

elasticsearch should be up and running.

Navigate to the folder containing kibana

`CD c:\kibana-7.8.1-windows-x86_64`

To start kibana

`.\bin\kibana.bat`

To stop kibana

`Ctrl-C`

Once Elasticsearch and kibana are installed clone the sophos-xdb using below command

`https://gitlab.com/sushmitha-hp/sophos-xdb.git`

Navigate to sophos-xdb folder and install requirements using below command

`pip install -r requirements.txt`

Then navigate to Students Score Card Analytics folder which is inside sopos-xdb folder.

`CD c:\sophos-xdb\Students Score Card Analytics`

Unittests for elasticserach written in test.py to run test file use below command.

`python -m unittest -v test.py`

Then run the main.py file

`python main.py`



- Refer Below Docs for furthur information:--

1. docs for elasticsearch (https://www.elastic.co/guide/en/elasticsearch/reference/current/zip-windows.html)
2. docs for kibana (https://www.elastic.co/guide/en/kibana/current/windows.html#install-windows)


