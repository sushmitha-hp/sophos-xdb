import unittest
import json
import requests
from unittest.mock import patch, mock_open
from main import get_student_data
from elastisearch import Elastisearch


class TestStudentdata(unittest.TestCase):

    def test_read_file(self):
        student_test_data = '{"maths_marks": 84, "name": "Shawn Hunter", "chemistry_marks": 90, \
            "result": "PASSED", "physics_marks": 76, "roll_no": 75}'
        file_path = './students_score_card.json'
        with patch(
            "builtins.open", 
            mock_open(read_data=student_test_data), 
            create=True) as json_file:
            
            result = get_student_data()
        
        json_file.assert_called_once_with(file_path, "r")
        self.assertEqual(result, json.loads(student_test_data))


class TestElastisearch(unittest.TestCase):

    def setUp(self):
        self.es = Elastisearch()
        self.index_name = "students_score_records"
        self.doc_type = "students"
        self.url = 'http://localhost:9200/students_score_records'

    def test_create_index(self):
        with patch('requests.put') as mock_put:
            
            mock_put.return_value.status_code = 200
            response = self.es.create_index(self.index_name)

            mock_put.assert_called_once_with(self.url)
            self.assertEqual(response.status_code, 200)
    
    def test_get_index(self):
        
        content = {"students_score_records": {"mappings": {"properties": {"chemistry_marks":\
        {"type":"long"},"maths_marks":{"type":"long"},"name":{"type":"text","fields":{"keyword":\
        {"type":"keyword","ignore_above":256}}},"physics_marks":{"type":"long"},"roll_no":{"type":"long"}}}}}
        
        with patch('requests.get') as mock_request:
            
            mock_request.return_value.content = content
            response = self.es.get_index(self.index_name)
            
            mock_request.assert_called_once_with(self.url)
            self.assertEqual(response, content)

    def test_populate_documents(self):  
        
        info = [{"maths_marks": 84, "name": "Shawn Hunter", "chemistry_marks": 90, 
            "result": "PASSED", "physics_marks": 76, "roll_no": 75}]

        message = f'Sucessfully added documents to Index :{self.index_name}'
        
        with patch('requests.post') as mock_post:
            
            mock_post.return_value = message

            response = self.es.populate_documents(self.index_name, self.doc_type, info)

            url = f'{self.url}/{self.doc_type}/0'
            mock_post.assert_called_with(
                url, 
                data=json.dumps(info[0]), 
                headers={'content-type': 'application/json'}
                )
            
            self.assertEqual(response, message)

    def test_delete_index(self):
        
        message = f'Sucessfully Deleted Index :{self.index_name}'
        
        with patch('requests.delete') as mock_request:
            
            mock_request.return_value.content = message
            response = self.es.delete_index(self.index_name)

            mock_request.assert_called_once_with(self.url)
            self.assertEqual(response, message)        

if __name__ == '__main__':
    unittest.main()


