import json
from elastisearch import Elastisearch

def get_student_data():
    """
    Extracts and returns students records from json file.
        returns:
            students_records (list): list of student details.
    """
    students_records = list()

    try:
        with open('./students_score_card.json','r') as json_file:
            students_records = json.load(json_file)
    except EnvironmentError as e:
        return e
    return students_records

# Python Builtin Module elasticsearch :
# from elasticsearch import helpers, Elasticsearch # builtin modules
# es_instance = Elasticsearch()

# def create_index(index_name):
#     """
#     Deleting index if it already exists and then
#     Creating newindex in elasticsearch.

#         Parameters:
#             index_name (str): name of index.
#         Returns:
#             sucess message (str)
    
#     """
#     try:
#         es.indices.delete(index = index_name, ignore=[400, 404])
#         es.indices.create(index = index_name,ignore=[400])
#     except Exception as e:
#         return e

#     return f'Sucessfully Created Index :{index_name}'

# def write_records(student_records):
#     """ Storing records in elasticsearch

#         Parameters:
#             student_records (list): list of student records.
#         Returns:
#             sucess message (str)

#     """
#     try:
#         for count, record in enumerate(student_records):
#             es.index(
#                 index=index_name,
#                 doc_type=doctype,
#                 id=count,
#                 body=record
#             )
#     except Exception as e:
#         return e
    
#     return f'Sucessfully added documents to Index :{index_name}'


if __name__ == '__main__':
    student_records = get_student_data()
    
    es = Elastisearch()
    index_name = 'students_score_records'
    doctype = 'students'

    response = json.loads(es.get_index(index_name))

    if index_name in response.keys():
        es.delete_index(index_name)
    
    response = es.create_index(index_name)

    if response.status_code == 200:
        print(es.populate_documents(index_name, doctype, student_records))
    
    # Below calls are done using python's elasticsearch module
    '''To create Index.'''
    # es_instance.create_index(index_name) #Builtin Module

    '''To populate records in Index.'''
    # es_instance.populate_documents(index_name,doc_type,students_records) #Builtin Module
    