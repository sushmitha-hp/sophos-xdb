import json
import requests

class Elastisearch:
    """ 
    This is a class for performing basic elasticsearch crud operations using REST Api. 
      
    Attributes: 
        base_url (str): url to acess elasticsearch
         
    """

    def __init__(self):
        self.base_url = 'http://localhost:9200/'

    def create_index(self, index_name):
        """ Creating newindex in elasticsearch

            Parameters:
                index_name (str): name of index
            Returns:
                response (obj)
        """
        try:
            response = requests.put(f'{self.base_url}{index_name}')
        except Exception as e:
            raise e

        return response
    
    def get_index(self, index_name):
        """ Fetching index from elasticsearch

            Parameters:
                index_name (str): name of index
            Returns:
                response (obj)
        """
        try:
            response = requests.get(f'{self.base_url}{index_name}')
        except Exception as e:
            raise e

        return response.content

    def populate_documents(self, index_name, doc_type, data):
        """ Storing records in elasticsearch

            Parameters:
                index_name (str): name of index
                doc_type (str): documnet type.
                data (list): list of student records.
            Returns:
                sucess message (str)
        """
        headers = {'content-type':'application/json'}
        url = f'{self.base_url}{index_name}/{doc_type}'
        
        try:
            for index, record in enumerate(data):
                response = requests.post(
                    f'{url}/{str(index)}',
                    data=json.dumps(record), 
                    headers=headers)
        except Exception as e:
            raise e
        
        return f'Sucessfully added documents to Index :{index_name}'

    def delete_index(self, index_name):
        """ Deleting index in elasticsearch

            Parameters:
                index_name (str): name of index
            Returns:
                sucess message (str)
        """
        try:
            response = requests.delete(f'{self.base_url}{index_name}')
        except Exception as e:
            raise e
        
        return f'Sucessfully Deleted Index :{index_name}'
